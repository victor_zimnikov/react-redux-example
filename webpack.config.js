const path = require('path');
const webpack = require("webpack");

module.exports = (values, { mode }) => {
  const DEV = mode === "development";

  return ({
    entry: [
      "@babel/polyfill",
      path.resolve(__dirname, 'src/index.jsx'),
    ],
    devtool: DEV ? "eval" : "source-map",
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js'
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env', '@babel/preset-react'],
              plugins: [require('@babel/plugin-proposal-object-rest-spread')]
            }
          }
        },
        {
          test: /\.css/,
          use: [
            "style-loader",
            "css-loader"
          ]
        },
        {
          test: /\.(png|jpg)$/,
          use: [
            {
              loader: 'file-loader',
            }
          ]
        }
      ]
    },

    plugins: DEV
      ? [
          DEV && new webpack.HotModuleReplacementPlugin(),
        ]
      : [],

    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      compress: true,
      port: 3000
    }
  });
};
