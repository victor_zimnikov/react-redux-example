import {
  camelCase,
  fromPairs,
  get,
  isEqualWith,
  isPlainObject,
  pick,
  toPairs,
  isEqual,
  map
} from "lodash-es";

export function camelCaseKeys(value) {
  return !isPlainObject(value)
    ? value
    : fromPairs(
        map(toPairs(value), ([k, v]) => [camelCase(k), camelCaseKeys(v)])
      );
}

function createShallowEqualCustomizer(depth = 1) {
  return (value, other, indexOrKey, parent, otherParent, stack) => {
    if (stack && stack.size > depth) {
      return value === other;
    }

    return false;
  };
}

export function isShallowEqual(a, b, depth) {
  return isEqualWith(a, b, createShallowEqualCustomizer(depth));
}

export function isEqualChild(a, b, key1, key2, key3, key4, key5, key6) {
  const path = [key1, key2, key3, key4, key5, key6].filter(x => x != null);
  const aChild = get(a, path);
  const bChild = get(b, path);

  return isEqual(aChild, bChild);
}

export function isEqualChildren(a, b, keys) {
  const aChildren = pick(a, keys);
  const bChildren = pick(b, keys);

  return isEqual(aChildren, bChildren);
}
