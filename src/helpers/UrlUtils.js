import { parse, stringify } from "querystring";
import { trim } from "lodash-es";

export function parseQuery(search) {
  return parse(trim(search, " ?\t\n"));
}

export function stringifyQuery(query) {
  return stringify(query);
}
