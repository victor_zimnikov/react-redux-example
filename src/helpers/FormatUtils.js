import { format } from "date-fns";

const DATE_PATTERN = "MMMM D, YYYY";

export function formatDate(date) {
  return format(date, DATE_PATTERN);
}
