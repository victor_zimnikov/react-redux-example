import { get, set, toPlainObject, unset } from "lodash-es";
import { camelCaseKeys } from "./DataUtils";

const HTTP_ERROR_FLAG = "$$HTTP_ERROR$$";
const HTTP_TIMEOUT_ERROR_FLAG = "$$HTTP_TIMEOUT_ERROR$$";
const HTTP_RESPONSE_ERROR_FLAG = "$$HTTP_RESPONSE_ERROR$$";

export function prepareHttpError(error) {
  // Axios errors always has configs.
  if (error.config) {
    set(error, HTTP_ERROR_FLAG, true);

    // We do not need request object anywhere.
    unset(error, "request");

    // Cleanup config object.
    unset(error.config, "cancelToken");
    unset(error.config, "xsrfCookieName");
    unset(error.config, "xsrfHeaderName");
    unset(error.config, "transformRequest");
    unset(error.config, "transformResponse");

    // Axios handles timeout errors with this code.
    if (error.code === "ECONNABORTED") {
      set(error, HTTP_TIMEOUT_ERROR_FLAG, true);
    }

    // Server returned response with error.
    if (error.response) {
      set(error, HTTP_RESPONSE_ERROR_FLAG, true);

      // Cleanup response object.
      unset(error.response, "config");
      unset(error.response, "request");
    }
  }
}

export function isHttpError(error) {
  return Boolean(get(error, HTTP_ERROR_FLAG));
}

export function isHttpTimeoutError(error) {
  return Boolean(get(error, HTTP_TIMEOUT_ERROR_FLAG));
}

export function isHttpResponseError(error) {
  return Boolean(get(error, HTTP_RESPONSE_ERROR_FLAG));
}

export function isHttpNetworkError(error) {
  return (
    isHttpError(error) &&
    (!isHttpTimeoutError(error) && !isHttpResponseError(error))
  );
}

export function formatHttpError(error) {
  if (!error) {
    return undefined;
  }

  if (isHttpResponseError(error)) {
    return get(error, ["response", "data", "non_field_errors", 0]);
  }

  if (isHttpTimeoutError(error)) {
    return "Request timeout.";
  }

  if (isHttpNetworkError(error)) {
    return "Connection error.";
  }

  return error.message;
}

export function formatHttpResponseErrors(error) {
  const response = error && get(error, ["response", "data"]);

  if (response) {
    return camelCaseKeys(toPlainObject(response));
  }

  return false;
}
