import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import ListContainer from "./ListContainer";
import AddContainer from "./AddContainer";
import { NotFoundContainer } from "./NotFoundContainer";

export class RootContainer extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={ListContainer} />
          <Route exact path="/add" component={AddContainer} />

          <Route component={NotFoundContainer} />
        </Switch>
      </BrowserRouter>
    );
  }
}
