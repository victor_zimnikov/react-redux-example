import "./assets/add-container.css";

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/es/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import IconButton from "@material-ui/core/es/IconButton";

import { Dialog } from "../components/ui/Dialog";
import { addUser } from "../actions/usersActions";
import { UserForm } from "../components/users/UserForm";
import { AppLayout } from "../components/app-layout/AppLayout";

const USER_CREATE_DIALOG_HASH = "#UCDH";

class AddContainer extends React.Component {
  render() {
    const { history, dispatch, location } = this.props;

    return (
      <AppLayout
        headerTitle="Add user"
        contentClassName="add-container-content"
        headerLeftComponent={
          <div className="back-button">
            <IconButton
              onClick={() => history.push("/")}
              color="inherit"
              aria-label="Back"
            >
              <ArrowBackIcon />
            </IconButton>
          </div>
        }
        headerRightComponent={
          <Button
            type="submit"
            onClick={() => {
              document
                .getElementById("user-form")
                .dispatchEvent(new Event("submit", { cancelable: true }));
              history.replace({ hash: "UCDH" });
            }}
            color="inherit"
            aria-label="Save"
          >
            <SaveIcon />
          </Button>
        }
      >
        <UserForm onSubmit={values => dispatch(addUser(values))} />

        <Dialog
          contentClassName="add-container-dialog-content"
          open={location.hash === USER_CREATE_DIALOG_HASH}
          onRequestClose={() => history.push({ pathname: "/", hash: "" })}
          actions={
            <Button onClick={() => history.push({ pathname: "/", hash: "" })}>
              OK
            </Button>
          }
        >
          <span className="add-container-dialog-text">
            User successfully created
          </span>
        </Dialog>
      </AppLayout>
    );
  }
}

AddContainer.propTypes = {
  dispatch: PropTypes.func,
  history: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  location: PropTypes.object // eslint-disable-line react/forbid-prop-types
};

export default connect()(AddContainer);
