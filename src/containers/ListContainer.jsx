import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import ImageIcon from "@material-ui/icons/Image";

import IconButton from "@material-ui/core/es/IconButton";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import ListItemSecondaryAction from "@material-ui/core/es/ListItemSecondaryAction";

import "./assets/list-container.css";
import Card from "@material-ui/core/es/Card";
import CardHeader from "@material-ui/core/es/CardHeader";
import CardContent from "@material-ui/core/es/CardContent";
import Button from "@material-ui/core/es/Button/Button";
import { Dialog } from "../components/ui/Dialog";
import { deleteUser } from "../actions/usersActions";
import { formatDate } from "../helpers/FormatUtils";
import { AppLayout } from "../components/app-layout/AppLayout";
import { parseQuery, stringifyQuery } from "../helpers/UrlUtils";

class ListContainer extends React.Component {
  render() {
    const { list, history, dispatch, query } = this.props;

    return (
      <AppLayout
        headerTitle="Users"
        headerRightComponent={
          <div>
            <IconButton
              onClick={() => history.push("/add")}
              color="inherit"
              aria-label="Add"
            >
              <AddIcon />
            </IconButton>
          </div>
        }
        contentClassName="list-container-content"
      >
        <Card className="list-container-card">
          <CardHeader
            title={list.length > 0 ? `Users (${list.length})` : "Users"}
          />
          <CardContent>
            {list.length === 0 && (
              <span className="list-container-empty-text">
                User list is empty
              </span>
            )}

            {list.length > 0 && (
              <List>
                {list.map(x => (
                  <ListItem key={x.id}>
                    <Avatar>
                      {x.photo ? (
                        <img src={x.photo} alt="user" />
                      ) : (
                        <ImageIcon />
                      )}
                    </Avatar>
                    <ListItemText
                      primary={x.fullName}
                      secondary={formatDate(x.birthday)}
                    />
                    <ListItemSecondaryAction>
                      <IconButton
                        onClick={() =>
                          history.replace({
                            search: stringifyQuery({ deleteId: x.id })
                          })
                        }
                        aria-label="Delete"
                      >
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            )}
          </CardContent>
        </Card>

        <Dialog
          open={Boolean(query.deleteId)}
          contentClassName="add-container-dialog-content"
          onRequestClose={() => history.push({ pathname: "/", hash: "" })}
          actions={
            <>
              <Button
                onClick={() => {
                  dispatch(deleteUser(query.deleteId));
                  history.replace({ search: "" });
                }}
              >
                OK
              </Button>
              <Button onClick={() => history.push({ hash: "" })}>CANCEL</Button>
            </>
          }
        >
          <span className="add-container-dialog-text">
            Are you sure you want to delete this user?
          </span>
        </Dialog>
      </AppLayout>
    );
  }
}

ListContainer.propTypes = {
  dispatch: PropTypes.func,
  list: PropTypes.array, // eslint-disable-line react/forbid-prop-types
  query: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  history: PropTypes.object // eslint-disable-line react/forbid-prop-types
};

export default connect(({ users }, { location }) => {
  const query = parseQuery(location.search);

  return {
    query,

    list: users.list
  };
})(ListContainer);
