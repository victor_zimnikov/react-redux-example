import React from "react";
import logger from "redux-logger";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import MuiPickersUtilsProvider from "material-ui-pickers/utils/MuiPickersUtilsProvider";

import { RootContainer } from "./RootContainer";
import { rootReducer } from "../store/rootReducer";

import "../theme/main.css";

export class AppContainer extends React.Component {
  render() {
    const store = createStore(rootReducer, applyMiddleware(logger));

    return (
      <Provider store={store}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <RootContainer />
        </MuiPickersUtilsProvider>
      </Provider>
    );
  }
}
