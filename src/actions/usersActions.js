import { ADD_USER, DELETE_USER } from "../reducers/UsersReducer";

export function addUser(values) {
  return {
    type: ADD_USER,
    payload: values
  };
}

export function deleteUser(id) {
  return {
    type: DELETE_USER,
    payload: id
  };
}
