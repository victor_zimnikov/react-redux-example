import React from "react";
import PropTypes from "prop-types";
import Card from "@material-ui/core/es/Card";
import CardContent from "@material-ui/core/es/CardContent";
import CardHeader from "@material-ui/core/es/CardHeader";
import { TextField } from "../form/TextField";
import { AppForm } from "../form/AppForm";

import "./assets/user-form.css";
import { SelectBoxField } from "../form/SelectBoxField";
import { DatePickerField } from "../form/DatePickerField";

const validateForm = values => {
  const errors = {};

  if (!values.firstName) {
    errors.firstName = "First name is required";
  }

  if (!values.firstName) {
    errors.firstName = "Last name is required";
  }

  if (!values.gender) {
    errors.gender = "Gender name is required";
  }

  if (!values.birthday) {
    errors.birthday = "Birthday name is required";
  }

  return errors;
};

export function UserForm(props) {
  return (
    <AppForm
      initialValues={{
        gender: "male",
        birthday: new Date()
      }}
      validate={validateForm}
      onSubmit={props.onSubmit}
      render={({ handleSubmit }) => (
        <form
          id="user-form"
          className="user-form-container"
          onSubmit={handleSubmit}
        >
          <Card className="user-form-card">
            <CardHeader title="General" />
            <CardContent className="user-form-card">
              <div className="user-form-row">
                <div className="user-form-cell">
                  <TextField
                    id="firstName"
                    name="firstName"
                    label="First Name"
                  />
                </div>
                <div className="user-form-cell">
                  <TextField id="lastName" name="lastName" label="Last Name" />
                </div>
              </div>
              <div className="user-form-row">
                <div className="user-form-cell">
                  <SelectBoxField
                    id="gender"
                    name="gender"
                    label="Gender"
                    data={[
                      {
                        label: "Male",
                        value: "male"
                      },
                      {
                        label: "Female",
                        value: "female"
                      }
                    ]}
                  />
                </div>
                <div className="user-form-cell">
                  <DatePickerField
                    id="birthday"
                    name="birthday"
                    label="Birthday"
                  />
                </div>
              </div>
            </CardContent>
          </Card>

          <Card className="user-form-card">
            <CardHeader title="Contacts" />
            <CardContent className="user-form-card">
              <div className="user-form-row">
                <div className="user-form-cell">
                  <TextField
                    id="email"
                    name="email"
                    label="Email"
                    type="email"
                  />
                </div>
                <div className="user-form-cell">
                  <TextField
                    id="phoneNumber"
                    name="phoneNumber"
                    label="Phone number"
                    type="tel"
                  />
                </div>
              </div>
            </CardContent>
          </Card>
        </form>
      )}
    />
  );
}

UserForm.propTypes = {
  onSubmit: PropTypes.func
};
