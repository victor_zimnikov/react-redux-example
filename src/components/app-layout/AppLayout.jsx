import React from "react";
import cx from "classnames";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { AppLayoutBar } from "./AppLayoutBar";

import "./assets/app-layout.css";

export function AppLayout(props) {
  return (
    <>
      <AppLayoutBar
        title={props.headerTitle}
        leftComponent={props.headerLeftComponent}
        rightComponent={props.headerRightComponent}
      />

      <Helmet>
        <title>{`React Example - ${props.headerTitle}`}</title>
      </Helmet>

      <div className="content-wrapper">
        <div className={cx("content", props.contentClassName)}>
          {props.children}
        </div>
      </div>
    </>
  );
}

AppLayout.propTypes = {
  children: PropTypes.node,

  contentClassName: PropTypes.string,

  headerLeftComponent: PropTypes.node,
  headerRightComponent: PropTypes.node,
  headerTitle: PropTypes.string.isRequired
};
