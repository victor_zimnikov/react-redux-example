import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import "./assets/app-layout-bar.css";

export function AppLayoutBar(props) {
  return (
    <AppBar>
      <Toolbar>
        {props.leftComponent}

        <Typography variant="title" color="inherit" className="typography">
          {props.title}
        </Typography>

        {props.rightComponent}
      </Toolbar>
    </AppBar>
  );
}

AppLayoutBar.propTypes = {
  title: PropTypes.string.isRequired,

  leftComponent: PropTypes.node,
  rightComponent: PropTypes.node
};
