import React from "react";
import PropTypes from "prop-types";
import MUIDialog from "@material-ui/core/es/Dialog";
import DialogTitle from "@material-ui/core/es/DialogTitle";
import DialogContent from "@material-ui/core/es/DialogContent";
import DialogActions from "@material-ui/core/es/DialogActions";

export function Dialog({
  open,
  onRequestClose,
  title,
  contentClassName,
  actions,
  children
}) {
  return (
    <MUIDialog
      open={open}
      onClose={onRequestClose}
      aria-labelledby="simple-dialog-title"
    >
      {title && <DialogTitle id="simple-dialog-title">{title}</DialogTitle>}

      <DialogContent className={contentClassName}>{children}</DialogContent>

      {actions && <DialogActions>{actions}</DialogActions>}
    </MUIDialog>
  );
}

Dialog.propTypes = {
  title: PropTypes.string,

  children: PropTypes.node.isRequired,

  contentClassName: PropTypes.string,

  actions: PropTypes.node,

  open: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired
};
