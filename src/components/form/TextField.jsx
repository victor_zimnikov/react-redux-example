import React from "react";
import PropTypes from "prop-types";
import Input from "@material-ui/core/es/Input";
import InputLabel from "@material-ui/core/es/InputLabel";
import FormControl from "@material-ui/core/es/FormControl";
import FormHelperText from "@material-ui/core/es/FormHelperText";
import { Field } from "react-final-form";

import "./assets/fields.css";

export function TextField(props) {
  return (
    <Field
      name={props.name}
      render={({ meta, input }) => {
        const hasError = Boolean(meta.touched && meta.invalid);

        return (
          <FormControl
            error={hasError}
            disabled={props.disabled}
            aria-describedby={
              props.disabled
                ? undefined
                : hasError
                  ? "name-error-text"
                  : "name-helper-text"
            }
          >
            {props.label && (
              <InputLabel htmlFor={props.id}>{props.label}</InputLabel>
            )}

            <Input {...input} type={props.type} id={props.id} />

            <div className="field-error-container">
              {hasError && (
                <FormHelperText id="name-error-text">
                  {meta.submitError || meta.error}
                </FormHelperText>
              )}
            </div>
          </FormControl>
        );
      }}
    />
  );
}

TextField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,

  disabled: PropTypes.bool,

  label: PropTypes.string,

  type: PropTypes.string
};
