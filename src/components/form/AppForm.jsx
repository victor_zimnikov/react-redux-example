import React from "react";
import { Form, FormSpy } from "react-final-form";
import { SetSubmitError, setSubmitErrorMutators } from "./SetSubmitError";
import {
  formatHttpError,
  formatHttpResponseErrors
} from "../../helpers/ErrorBuilder";

const appFormMutators = {
  ...setSubmitErrorMutators
};

export function AppForm(props) {
  const {
    render,

    children,
    component,

    mutators,
    submitError,

    onChange,
    changeSubscription,

    ...restProps
  } = props;

  if (children) {
    throw new Error("AppForm: `children` is not supported.");
  }

  if (component) {
    throw new Error("AppForm: `component` is not supported.");
  }

  if (!render) {
    throw new Error("AppForm: `render` is required.");
  }

  return (
    <Form
      {...restProps}
      mutators={{ ...appFormMutators, ...mutators }}
      render={formProps => (
        <>
          <SetSubmitError
            form={formProps.form}
            extraProps={{ submitError }}
            submitError={formatHttpError(submitError)}
            submitErrors={formatHttpResponseErrors(submitError)}
          />

          <FormSpy
            onChange={onChange}
            component={() => null}
            subscription={changeSubscription}
          />

          {render(formProps)}
        </>
      )}
    />
  );
}
