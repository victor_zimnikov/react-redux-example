import React from "react";
import PropTypes from "prop-types";
import InputLabel from "@material-ui/core/es/InputLabel";
import FormControl from "@material-ui/core/es/FormControl";
import FormHelperText from "@material-ui/core/es/FormHelperText";
import { Field } from "react-final-form";
import { DatePicker } from "material-ui-pickers";
import Input from "@material-ui/core/es/Input";

import "./assets/fields.css";

export function DatePickerField(props) {
  return (
    <Field
      name={props.name}
      render={({ meta, input }) => {
        const hasError = Boolean(meta.touched && meta.invalid);

        return (
          <FormControl error={hasError} disabled={props.disabled}>
            {props.label && (
              <InputLabel htmlFor={props.id}>{props.label}</InputLabel>
            )}

            <DatePicker
              {...input}
              TextFieldComponent={({
                InputProps,
                helperText,
                ...inputProps
              }) => (
                <Input
                  {...inputProps}
                  id={props.id}
                  disabled={props.disabled}
                />
              )}
            />

            <div className="field-error-container">
              {hasError && (
                <FormHelperText>
                  {meta.submitError || meta.error}
                </FormHelperText>
              )}
            </div>
          </FormControl>
        );
      }}
    />
  );
}

DatePickerField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,

  disabled: PropTypes.bool,

  label: PropTypes.string
};
