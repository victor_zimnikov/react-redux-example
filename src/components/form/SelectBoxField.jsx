import React from "react";
import hash from "object-hash";
import PropTypes from "prop-types";
import Input from "@material-ui/core/es/Input";
import InputLabel from "@material-ui/core/es/InputLabel";
import FormControl from "@material-ui/core/es/FormControl";
import FormHelperText from "@material-ui/core/es/FormHelperText";
import { Field } from "react-final-form";
import NativeSelect from "@material-ui/core/es/NativeSelect";

import "./assets/fields.css";

export function SelectBoxField(props) {
  return (
    <Field
      name={props.name}
      render={({ meta, input }) => {
        const hasError = Boolean(meta.touched && meta.invalid);

        return (
          <FormControl error={hasError} disabled={props.disabled}>
            {props.label && (
              <InputLabel htmlFor={props.id}>{props.label}</InputLabel>
            )}

            <NativeSelect
              {...input}
              name={props.name}
              input={<Input id={props.id} />}
            >
              {props.data.map(x => (
                <option key={hash(x)} value={x[props.valueKey]}>
                  {x[props.labelKey]}
                </option>
              ))}
            </NativeSelect>

            <div className="field-error-container">
              {hasError && (
                <FormHelperText>
                  {meta.submitError || meta.error}
                </FormHelperText>
              )}
            </div>
          </FormControl>
        );
      }}
    />
  );
}

SelectBoxField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,

  disabled: PropTypes.bool,

  label: PropTypes.string,

  labelKey: PropTypes.string,
  valueKey: PropTypes.string,

  data: PropTypes.arrayOf(PropTypes.object).isRequired
};

SelectBoxField.defaultProps = {
  labelKey: "label",
  valueKey: "value"
};
