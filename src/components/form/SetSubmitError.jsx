import React from "react";
import { set } from "lodash-es";
import PropTypes from "prop-types";
import { isEqualChildren, isShallowEqual } from "../../helpers/DataUtils";

export const setSubmitErrorMutators = {
  setSubmitError: ([submitError, submitErrors], state, { setIn }) => {
    if (submitError || submitErrors) {
      set(state, "formState", setIn(state.formState, "submitting", false));
      set(state, "formState", setIn(state.formState, "submitFailed", true));
      set(state, "formState", setIn(state.formState, "submitSucceeded", false));

      set(
        state,
        "formState",
        setIn(state.formState, "submitError", submitError)
      );

      set(
        state,
        "formState",
        setIn(state.formState, "submitErrors", submitErrors)
      );
    }
  }
};

export class SetSubmitError extends React.Component {
  constructor(props) {
    super(props);

    this.frame = null;
  }

  componentDidMount() {
    this.notifyForm();
  }

  componentDidUpdate(prevProps) {
    const { extraProps } = this.props;

    if (
      !isShallowEqual(extraProps, prevProps.extraProps) ||
      !isEqualChildren(this.props, prevProps, ["submitError", "submitErrors"])
    ) {
      this.notifyForm();
    }
  }

  cancelAnimationFrame() {
    if (this.frame) {
      cancelAnimationFrame(this.frame);
    }
  }

  notifyForm() {
    const { form, submitError, submitErrors } = this.props;

    this.cancelAnimationFrame();

    if (submitError || submitErrors) {
      this.frame = requestAnimationFrame(() => {
        form.mutators.setSubmitError(submitError, submitErrors);
      });
    }
  }

  render() {
    return null;
  }
}

SetSubmitError.propTypes = {
  form: PropTypes.any, // eslint-disable-line react/forbid-prop-types
  extraProps: PropTypes.any, // eslint-disable-line react/forbid-prop-types
  submitError: PropTypes.any, // eslint-disable-line react/forbid-prop-types
  submitErrors: PropTypes.any // eslint-disable-line react/forbid-prop-types
};
