import { combineReducers } from "redux";
import { usersReducer } from "../reducers/UsersReducer";

export const rootReducer = combineReducers({
  users: usersReducer
});
