import hash from "object-hash";
import { formatDate } from "../helpers/FormatUtils";

export const ADD_USER = "USERS/ADD_USER";
export const DELETE_USER = "USERS/DELETE_USER";

const defaultState = {
  list: [
    {
      id: "9fc3b764a211d0b1ff88b2ae65aeb053e66de4bf",
      firstName: "John",
      lastName: "Smith",
      fullName: "John Smith",
      birthday: new Date(
        "Sun Jun 11 1978 00:00:00 GMT+0300 (Eastern European Standard Time)"
      ),
      gender: {
        label: "Male",
        value: "male"
      },
      photo: "/assets/users/john_smith.jpg"
    },
    {
      id: "9fc3b764a211d0b1ff88b2ae65aeb053e3466de4bf",
      firstName: "Victor",
      lastName: "Zimnikov",
      fullName: "Victor Zimnikov",
      birthday: new Date(
        "Sat Aug 16 1986 00:00:00 GMT+0300 (Eastern European Standard Time)"
      ),
      gender: {
        label: "Male",
        value: "male"
      },
      photo: "/assets/users/victor_zimnikov.jpg"
    }
  ]
};

export const usersReducer = (state = defaultState, { type, payload }) => {
  switch (type) {
    case ADD_USER: {
      const { list } = state;

      const id = hash(
        `${payload.firstName}-${payload.lastName}-${
          payload.gender.value
        }-${formatDate(payload.birthday)}`
      );
      const fullName = [payload.firstName, payload.lastName].join(" ");

      list.push({
        ...payload,

        id,
        fullName
      });

      return {
        ...state,

        list
      };
    }

    case DELETE_USER: {
      const list = state.list.filter(x => x.id !== payload);

      return {
        ...state,

        list
      };
    }

    default:
      return state;
  }
};
