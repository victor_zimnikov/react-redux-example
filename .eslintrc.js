module.exports = {
  "root": true,
  "env": { "es6": true, "node": true, "browser": true },
  "parser": "babel-eslint",
  "plugins": ["babel", "react"],
  "extends": ["airbnb", "prettier", "prettier/flowtype", "prettier/react"],
  "rules": {
    "import/prefer-default-export": 0,
    "react/destructuring-assignment": 0,
    "react/require-default-props": 0,
    "no-nested-ternary": 0,
    "react/prefer-stateless-function": 0
  }
};